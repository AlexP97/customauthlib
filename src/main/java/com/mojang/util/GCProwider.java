package com.mojang.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GCProwider {

    /**
     * Связь с моей либкой
     */
    public static String getResourceString(String key, String def) {
        Class<?> target;
        try {
            target = Class.forName("ru.alexp.gc.util.Client");
            final Method method = target.getMethod("getString", String.class);
            return method.invoke(null, key).toString();
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ignore) {
        }
        return def;
    }
}
