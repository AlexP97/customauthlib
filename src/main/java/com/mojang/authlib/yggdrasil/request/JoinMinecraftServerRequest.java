/*
 * Decompiled with CFR 0_119.
 */
package com.mojang.authlib.yggdrasil.request;

import java.util.UUID;

public class JoinMinecraftServerRequest {
    public String accessToken;
    public UUID selectedProfile;
    public String serverId;
}

